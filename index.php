<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejercicio 5</title>
	<style type="text/css">
		table {
			width: 10%;
			height: 300px;
			margin-left: auto;
			margin-right: auto;
			border-collapse: collapse;
			border: 1px solid black;
		}
		caption {
			font-weight: bold;
			background-color: lightblue;
			border: 1px solid black;
			border-bottom: none;
		}
		tr:nth-child(odd) {
			background: gray;
		}

		tr:nth-child(even) {
			background: white;
		}
		td {
			padding: 5px;
			text-align: center;
		}
	</style>
</head>
<body>
	<?php 
		$nuevex1 = 9*1;
		$nuevex2 = 9*2;
		$nuevex3 = 9*3;
		$nuevex4 = 9*4;
		$nuevex5 = 9*5;
		$nuevex6 = 9*6;
		$nuevex7 = 9*7;
		$nuevex8 = 9*8;
		$nuevex9 = 9*9;
		$nuevex10 = 9*10;
		echo(<<< EOT
			<table>
		<caption>Tabla de Multiplicar del 9</caption>
		<tr>
			<td>9x</td>
			<td>1 =</td>
			<td>$nuevex1</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>2 =</td>
			<td>$nuevex2</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>3 =</td>
			<td>$nuevex3</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>4 =</td>
			<td>$nuevex4</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>5 =</td>
			<td>$nuevex5</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>6 =</td>
			<td>$nuevex6</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>7 =</td>
			<td>$nuevex7</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>8 =</td>
			<td>$nuevex8</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>9 =</td>
			<td>$nuevex9</td>
		</tr>
		<tr>
			<td>9x</td>
			<td>10 =</td>
			<td>$nuevex10</td>
		</tr>

	</table>
	EOT);
	 ?>
</body>
</html>